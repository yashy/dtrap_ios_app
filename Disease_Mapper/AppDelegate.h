//
//  AppDelegate.h
//  Disease_Mapper
//
//  Created by yash yadav on 1/3/16.
//  Copyright © 2016 yash yadav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

