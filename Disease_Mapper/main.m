//
//  main.m
//  Disease_Mapper
//
//  Created by yash yadav on 1/3/16.
//  Copyright © 2016 yash yadav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
